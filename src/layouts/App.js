import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { Provider } from 'react-redux';
import store from '../data/store';
import { Router, Route } from 'react-router-dom';
import history from '../helpers/history';
import login from '../pages/auth/login';
import Online from '../pages/online';
import { PrivateRoute } from '../Routes/PrivateRoute';
import Home from '../pages/home';
import '../assets/style/index.scss';

function App() {
  AOS.init({
    duration: 800,
    disable: 'phone',
  });
  return (
    <Provider store={store}>
      <Router history={history}>
        <div>
          <Route exact path="/" component={Home} />
          <PrivateRoute exact path="/online" component={Online} />
          <Route path="/login" component={login} />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
