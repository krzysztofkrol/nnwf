import React from 'react';
import '../assets/style/Footer.scss';
import semantika from '../assets/images/semantika.png';
import magazynTrenera from '../assets/images/magazynTrenera.png';

const Footer = () => {
  return (
    <>
      {/* <section className="order">
    <a  href="http://kindofmusic.pl/" className="order__btn">
      ZAMÓW
    </a>
     <h2 className="offer">WKRÓTCE W SPRZEDAŻY</h2>
  </section> */}
      <footer className="footer">
        <div className="container">
          <div className="footer__left-info">
            <h3>Wydawca</h3>
            <img src={semantika} alt="" />
            <p>ul. Żuławska 10, 60-412 Poznań</p>
            <p>KRS: 0000470869, Sąd Rejonowy w Poznaniu</p>
            <p>VIII Wydział Gospodarczy KRS</p>
            <p>NIP: 7773232650, Regon: 302483298</p>
          </div>

          <div className="footer__contact">
            <h3>Kontakt</h3>
            <p>Skontaktuj się z nami</p>
            <p>
              <b>
                <i className="fas fa-envelope"> </i> biuro@semantika.pl
              </b>
            </p>
            <p>
              <b>
                <i className="fas fa-phone-square-alt"> </i> <span>61 847 11 34</span>
              </b>
            </p>
            <p>
              <b>
                <i className="fas fa-globe-europe"> </i>{' '}
                <a href="https://semantika.pl/" target="_blank" rel="noopener noreferrer">
                  www.semantika.pl
                </a>
              </b>
            </p>
          </div>
          <div className="footer__left">
            <div className="footer__left-logo">
              <h3>Więcej</h3>
              <div className="footer__left-facebook">
                <a
                  href="https://www.facebook.com/kreatywnynauczycielwf/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-facebook"> </i>
                  <span>Odzwiedź nas na Facebooku</span>
                </a>
                <a href="https://wychowanie-fizyczne.pl/" target="_blank" rel="noopener noreferrer">
                  <img src={magazynTrenera} alt="" />
                  <span>Wychowanie Fizyczne i Opieka Trenerska</span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__copyright">
          Copyright &copy; Grupa Wydawnicza Semantika 2020. Wszystkie prawa zastrzeżone
        </div>
      </footer>
    </>
  );
};
export default Footer;
