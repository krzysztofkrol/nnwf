import React, { Component } from 'react';
import '../assets/style/Navigation.scss';
import { Link as ScrollLink } from 'react-scroll';
import MenuButton from '../components/MenuButton';
import { Link } from 'react-router-dom';
import logo from '../assets/images/logo.png';
import StickyNav from 'react-stickynode';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      condition: !this.state.condition,
    });
  }

  render() {
    return (
      <StickyNav enabled={true} top={0} innerZ={999} activeClass={'sticky'}>
        <nav id="nav" className={this.state.condition ? 'nav open' : 'nav'}>
          <MenuButton className="menu" toggleClassName={this.handleClick} />
          <div className="container">
            <div className="brand">
              <ScrollLink to="home" smooth offset={-70}>
                <img src={logo} alt="" />
              </ScrollLink>
            </div>

            <ul className="navbar">
              <li>
                <ScrollLink to="start" smooth offset={-70}>
                  <i className="fas fa-home" />
                  Start
                </ScrollLink>
              </li>
              <li>
                <ScrollLink to="about" smooth offset={-10}>
                  Organizacja pracy
                </ScrollLink>
              </li>
              <li>
                <ScrollLink to="films" smooth offset={0}>
                  Filmy
                </ScrollLink>
              </li>
              <li>
                <ScrollLink to="materials" smooth offset={-70}>
                  Scenariusze
                </ScrollLink>
              </li>
              <li>
                <a
                  className="bold"
                  href="https://sklep.semantika.pl/products/niezbednik-nauczyciela-wf"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Zamów
                </a>
              </li>
              <li>
                <Link to={'/online'}>Video</Link>
              </li>
            </ul>
          </div>
        </nav>
      </StickyNav>
    );
  }
}

export default Navigation;
