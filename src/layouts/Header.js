import React from 'react';
import '../assets/style/Header.scss';
import { Link } from 'react-scroll';

const Header = () => {
  return (
    <header id="home">
      <h1>PRAKTYCZNY. NOWOCZESNY. NIEZNISZCZALNY.</h1>
      <div className="border-title">
        <h2>Niezbędnik Nauczyciela WF 2020-2021</h2>
      </div>
      <p>Najchętniej kupowany przez nauczycieli wychowania fizycznego w 2018 i 2019 roku</p>
      {/* <h2 className="offer">WKRÓTCE W SPRZEDAŻY</h2> */}
      <Link to="start" smooth={true} offset={-70}>
        <span> </span>
        <span> </span>
        <span> </span>
      </Link>
    </header>
  );
};
export default Header;
