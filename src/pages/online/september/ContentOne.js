import React from 'react';
import '../../../assets/style/video/content.scss';
import one from '../../../assets/images/s1/3-1.jpg';

const ContentOne = () => {
  return (
    <section className="content">
      <div className="content__head">
        <h2>Wzmacniamy więzi- integracyjne gry i zabawy bieżne</h2>
        <h3>IV etap edukacyjny</h3>
        <p>
          Proste zabawy bieżne, proponowane w tym scenariuszu, dobrze sprawdzą się w klasach
          licealnych. Słowem – przedstawiamy zastosowanie prostych przyborów, które nie wymagają od
          szkoły nakładów finansowych, a mogą znacząco wzmocnić więzi w grupie.
        </p>
      </div>
      <div className="content__video">
        <img src={one} alt="" />
        <div className="content__video-nav">
          <div className="left">
            <i className="fas fa-caret-left"></i>
            poprzedni
          </div>
          <div className="left">
            następny
            <i className="fas fa-caret-right"></i>
          </div>
        </div>
      </div>
      <div className="content__description">
        <h3>Pozycja wyjściowa</h3>
        <p>1</p>
        <h3>Ruch</h3>
        <p>2</p>
        <h3>Bezpieczeństwo</h3>
        <p>3</p>
      </div>
    </section>
  );
};

export default ContentOne;
