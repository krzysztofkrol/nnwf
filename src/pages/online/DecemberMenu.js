import React from "react";
import { Link } from "react-router-dom";

const DecemberMenu = () => {
  return (
    <div className="video__menu">
      <div className="video__menu-head">
        <h2>Scenariusze - Grudzień</h2> <button>–</button>
      </div>
      <div className="video__menu-list">
        <Link to={"/video/scenariusze/zimowy-scenariusz-lekcji"}>
          Zimowy scenariusz lekcji WF – gry i ćwiczenia hartujące organizm
        </Link>
        <Link to={"/video/scenariusze/kreatywny-scenariusz"}>
          Kreatywny scenariusz – czyli co zrobić kiedy brakuje sprzętu
        </Link>
        <Link to={"/video/scenariusze/stawiamy-na-fitness"}>
          Stawiamy na fitness!
        </Link>
      </div>
    </div>
  );
};

export default DecemberMenu;
