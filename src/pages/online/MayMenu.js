import React from "react";
import { Link } from "react-router-dom";

const MayMenu = () => {
  return (
    <div className="video__menu">
      <div className="video__menu-head">
        <h2>Scenariusze - Maj</h2> <button>–</button>
      </div>
      <div className="video__menu-list">
        <Link to={"/video/scenariusze/lekkoatletyka-w-nowej-odslonie"}>
          Lekkoatletyka w nowej odsłonie
        </Link>
        <Link to={"/video/scenariusze/kolorowa-rywalizacja"}>
          Kolorowa rywalizacja – zawody sportowe
        </Link>
        <Link to={"/video/scenariusze/igrzyska-sportowe"}>
          Kolonijne igrzyska sportowe
        </Link>
      </div>
    </div>
  );
};

export default MayMenu;
