import React from 'react';
import GoUp from '../../components/GoUp';
import Navigation from './Navigation';
import Sidebar from './Sidebar';
import Content from './Content';
import '../../assets/style/video/video.scss';
import Footer from '../../layouts/Footer';
import ScrollUpButton from 'react-scroll-up-button';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ContentOne from './september/ContentOne';
import history from '../../helpers/history';

function Online() {
  return (
    <>
      <Navigation />
      <main className="video">
        <Router history={history}>
          <Sidebar />
          <Switch>
            <Route
              path="/online/video/pilates/:number/:slug"
              component={(props) => <Content {...props} />}
            />
            <Route
              path="/online/video/samoobrona/:number/:slug"
              component={(props) => <Content {...props} />}
            />

            <Route exact path="/online" component={Content} />
            <Route path="/online/wzmacniamy-wiezi" component={ContentOne} />
          </Switch>
        </Router>
      </main>
      <Footer />
      <ScrollUpButton
        ContainerClassName="AnyClassForContainer"
        TransitionClassName="AnyClassForTransition"
        EasingType="linear"
        ShowAtPosition={200}
      >
        <GoUp />
      </ScrollUpButton>
    </>
  );
}

export default Online;
