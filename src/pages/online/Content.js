import React, { Component } from 'react';
import '../../assets/style/video/content.scss';
import { withRouter } from 'react-router-dom';
import { getVideo } from '../../data/actions/video.action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Content extends Component {
  componentDidMount(prevProps) {
    const { getVideo } = this.props;

    if (this.props.match.params.number) {
      getVideo(this.props.match.params.number);
    } else {
      getVideo(0);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.match.url !== prevProps.match.url) {
      const { getVideo } = this.props;
      getVideo(this.props.match.params.number);
    }
  }

  render() {
    let { video } = this.props;
    video = video.video;
    return (
      <section className="content">
        <div className="content__head">
          <h2>
            {video.category} <i className="fas fa-caret-down"></i>
          </h2>
          <h3>{video.title}</h3>
          <p>{video.description}</p>
        </div>
        <div className="content__video">
          <iframe
            src={video.video}
            width="640"
            height="360"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen
          ></iframe>
          <div className="content__video-nav">
            <div className="left">
              <i className="fas fa-caret-left"></i>
              poprzedni
            </div>
            <div className="left">
              następny
              <i className="fas fa-caret-right"></i>
            </div>
          </div>
        </div>
        <div className="content__description">
          <h3>Pozycja wyjściowa</h3>
          <p>{video.pozycja}</p>
          <h3>Ruch</h3>
          <p>{video.ruch}</p>
          <h3>Bezpieczeństwo</h3>
          <p>{video.bezpieczenstwo}</p>
        </div>
      </section>
    );
  }
}
const mapStateToProps = (state) => {
  const { video } = state;
  return { video };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getVideo: getVideo,
    },
    dispatch,
  );
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Content));
