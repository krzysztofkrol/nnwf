import React from "react";
import { Link } from "react-router-dom";

const SemptemberMenu = () => {
  return (
    <div className="video__menu">
      <div className="video__menu-head">
        <h2>Scenariusze - Wrzesień</h2> <button>–</button>
      </div>
      <div className="video__menu-list">
        <Link to={"/online/wzmacniamy-wiezi"}>
          Wzmacniamy więzi- integracyjne gry i zabawy bieżne
        </Link>
        <Link to={"/online/scenariusze/doskonalenie-pracy-nog"}>
          Doskonalenie pracy nóg w koszykówce
        </Link>
        <Link to={"/online/scenariusze/w-rytmie-muzyki"}>W rytmie muzyki</Link>
      </div>
    </div>
  );
};

export default SemptemberMenu;
