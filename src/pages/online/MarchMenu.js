import React from "react";
import { Link } from "react-router-dom";

const MarchMenu = () => {
  return (
    <div className="video__menu">
      <div className="video__menu-head">
        <h2>Scenariusze - Marzec</h2> <button>–</button>
      </div>
      <div className="video__menu-list">
        <Link to={"/video/scenariusze/zabawy-wiosenne"}>Zabawy wiosenne</Link>
        <Link to={"/video/scenariusze/gry-i-zabawy"}>
          Gry i zabawy z elementami piłki nożnej
        </Link>
        <Link to={"/video/scenariusze/sprawdzian-z-siatkowki"}>
          Sprawdzian z siatkówki na 6!
        </Link>
      </div>
    </div>
  );
};

export default MarchMenu;
