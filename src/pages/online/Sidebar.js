import React from 'react';
import '../../assets/style/video/sidebar.scss';

import SemptemberMenu from './SeptemberMenu';
import DecemberMenu from './DecemberMenu';
import MarchMenu from './MarchMenu';
import MayMenu from './MayMenu';
import PilatesMenu from '../../components/PilatesMenu';
import SamoobronaMenu from '../../components/SamoobronaMenu';

const Sidebar = () => {
  return (
    <section className="sidebar">
      <PilatesMenu />
      <SamoobronaMenu />
      <SemptemberMenu />
      <DecemberMenu />
      <MarchMenu />
      <MayMenu />
    </section>
  );
};
export default Sidebar;
