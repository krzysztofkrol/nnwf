import React from 'react';
import '../../assets/style/video/navigation.scss';
import logo from '../../assets/images/logo.png';

const Navigation = (props) => {
  return (
    <nav className="nav-video">
      <div className="nav-video__logo">
        <img src={logo} alt="" />
      </div>
      <div className="nav-video__log-out">
        {/* <Button onClick={logOut}>Log out</Button> */}
        <p>krzysztof.oleszkiewicz@semantika.pl</p>
        <button
        // onClick={logOut}
        >
          Wyloguj się
        </button>
      </div>
    </nav>
  );
};
export default Navigation;
