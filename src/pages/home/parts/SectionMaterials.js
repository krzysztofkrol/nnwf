import React from 'react';
import '../../../assets/style/Materials.scss';
import scenariusze from '../../../assets/images/scenariusze.jpg';
import scenariuszePion from '../../../assets/images/scenariusze-pion.jpg';
import scenariuszeChoice from '../../../assets/images/scenariusze-wybor.jpg';

const SectionMaterials = () => {
  return (
    <section id="materials" className="materials">
      <div className="container">
        <div className="materials__image">
          <img src={scenariusze} alt="" />
        </div>
        <div className="materials__text">
          <h2>
            <span>Scenariusze lekcji</span> do pobrania
          </h2>
          <p>
            <b>zestaw 12 autorskich scenariuszy lekcji na każdą porę roku</b> - dostępne na
            dedykowanej platformie konspekty zgodne z podstawą programową. Można je dowolnie łączyć
            i modyfikować, dostosowując do różnych poziomów nauczania.
          </p>
        </div>
        <div data-aos="fade-right" className="materials__material__vertical">
          <img src={scenariuszePion} alt="" />
        </div>
        <div data-aos="fade-up" data-aos-delay="800" className="materials__material__choice">
          <img src={scenariuszeChoice} alt="" />
        </div>

        <div className="materials__gray"> </div>

        <div className="materials__blue"> </div>
      </div>
    </section>
  );
};
export default SectionMaterials;
