import React from 'react';
import '../../../assets/style/About.scss';
import wideo from '../../../assets/images/wideo.jpg';
import calendar from '../../../assets/images/kalendarz.png';

const SectionAbout = () => {
  return (
    <section id="films" className="about">
      <div className="container">
        <div className="about__text">
          <div className="about__text-border">
            <h2>Co Nowego?</h2>
            <span>w edycji 2020/2021</span>
          </div>

          <h3>Filmy instruktażowe i zdjęcia</h3>
          <p>
            <b>gotowe propozycje nietypowych ćwiczeń</b> na lekcje WF
          </p>
          <p>oraz zdjęcia i filmy instruktażowe z zakresu:</p>
          <ul>
            <li>samoobrony</li>
            <li>cross-treningu</li>
            <li>Pilatesu</li>
          </ul>
          <p>opatrzone komentarzem eksperta, dostosowane do </p>
          <p> możliwości uczniów na różnych poziomach wiekowych</p>
        </div>
        <div data-aos="fade-left" className="about__calendar">
          <img src={calendar} alt="" />
        </div>
        <div data-aos="fade-up" className="about__video">
          <img src={wideo} alt="" />
        </div>
      </div>
    </section>
  );
};
export default SectionAbout;
