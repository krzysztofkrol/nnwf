import React from 'react';
import '../../../assets/style/Buy.scss';
import imageLeft from '../../../assets/images/promo-left.png';

const SectionBuy = () => (
  <section id="buy" className="buy">
    <div className="container">
      <div data-aos="fade-right" className="buy__left">
        <img src={imageLeft} alt="" />
      </div>
      <div data-aos="fade-left" className="buy__right">
        <div className="buy__right-title">
          <h3>NIEZBĘDNIK</h3>
          <h2>NAUCZYCIELA WF</h2>
        </div>
        {/* <p className="right-title-bottom">teraz w przedsprzedaży</p> */}
        <div className="buy__right-list">
          <div className="list-element">
            <div className="element-text">
              <p>
                <b>Zamów już teraz i zarezerwuj nowy Niezbędnik z wszystkimi dodatkami.</b>
              </p>
            </div>
          </div>
        </div>
        <div className="buy__price">
          <div className="price-promo">
            <h3>
              <b>
                <span>CENA</span> CENA na początek wakacji:
              </b>
            </h3>
            <h2>
              <b>
                69
                <small>zł </small>
              </b>
              <span>
                79
                <small>zł</small>
              </span>
            </h2>
          </div>
          <div className="price-order">
            <a
              href="https://sklep.semantika.pl/products/niezbednik-nauczyciela-wf"
              target="_blank"
              rel="noopener noreferrer"
              className="order-btn"
            >
              ZAMÓW
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default SectionBuy;
