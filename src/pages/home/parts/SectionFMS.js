import React from 'react';

import '../../../assets/style/FMS.scss';
import imageOne from '../../../assets/images/fms-one.jpg';
import imageTwo from '../../../assets/images/fms-two.jpg';

const SectionFMS = () => {
  return (
    <section className="fms">
      <div className="container">
        <div className="fms__text">
          <h2>
            TEST FMS <span>- Functional Movement Screen</span>
          </h2>
          <p>
            stosowany do diagnozy treningów pod wybraną dyscyplinę sportową lub gry zespołowe, w
            których uczniowie biorą udział m.in.: w rozgrywkach międzyszkolnych - zwizualizowany
            praktycznymi zdjęciami
          </p>
        </div>
        <div className="fms__image-one">
          <img src={imageOne} alt="" />
        </div>
        <div data-aos="fade-left" className="fms__image-two">
          <img src={imageTwo} alt="" />
        </div>
        <div data-aos="fade-left" className="fms__gray-box">
          {' '}
        </div>
      </div>
    </section>
  );
};
export default SectionFMS;
