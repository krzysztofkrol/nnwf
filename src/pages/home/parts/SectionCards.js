import React from 'react';
import SliderOne from '../../../components/SliderOne';
import SliderTwo from '../../../components/SliderTwo';
import SliderThree from '../../../components/SliderThree';
import '../../../assets/style/Cards.scss';

const SectionCards = () => {
  return (
    <section className="cards">
      <div className="cards__panel cards_panel-one">
        <div className="cards__panel-text">
          <h2>ORGANIZACJA PRACY</h2>
          <h3>kalendarz na rok szkolny 2020/2021</h3>
          <h3>plany lekcji - tylko dla nauczycieli WF</h3>

          <p>kalendarz skrócony</p>
          <p className="cards__panel-border">
            <span>NOWOŚĆ!</span>
          </p>
          <p>rozbudowany i obszerniejszy kalendarz miesięczny z planem roku szkolnego</p>
          <p>- dodatkowa 1 strona na miesiąc,</p>
          <p>
            <b>więcej miejsca na ważne notatki</b>
          </p>
          <p className="cards__panel-padding">plany lekcji na oba półrocza</p>
          <p className="cards__panel-padding">
            <b>wszystkie ważne wydarzenia i terminy</b>
          </p>
          <p className="cards__panel-padding">
            <b>będziesz mieć w jednym miejscu,</b> razem z kalendarzem będziesz mieć zawsze pod ręką
            swój <b>plan lekcji na każdy semestr</b>!
          </p>
        </div>
        <div className="cards__panel-image">
          <SliderOne />
        </div>
      </div>
      <div className="cards__panel cards_panel-two">
        <div className="cards__panel-text">
          <h2>ORGANIZACJA KLAS</h2>
          <h3>praktyczne, szerokie tabele</h3>
          <h3>przygotowane dla 8 klas</h3>

          <p>
            <b>BLOK każdej klasy zawiera:</b>
          </p>
          <ul>
            <li>
              tabelę <b>Frekwencja uczniów NOWOŚĆ!</b> Teraz <b>dodatkowa pozycja na zapis daty</b>{' '}
              wykonywanej lekcji WF
            </li>
            <li>
              tabelę <b>Tematy zajęć</b>
            </li>
            <li>
              tabelę <b>Oceny uczniów</b>
            </li>
            <li>
              tabelę <b>Wyniki sprawdzianów i testów sprawności fizycznej</b>
            </li>
            <li>
              tabelę <b>Plan sprawdzianów umiejętności</b>
            </li>
            <li>
              tabelę <b>Dodatkowe informacje o uczniach</b>
            </li>
            <li>
              przestrzeń <b>notatki</b>
            </li>
          </ul>
        </div>
        <div className="cards__panel-image">
          <SliderTwo />
        </div>
      </div>
      <div className="cards__panel cards_panel-three">
        <div className="cards__panel-text">
          <h2>WARSZTAT WUEFISTY</h2>
          <h3>kalendarz zawodów szkolnych</h3>
          <h3>testy sprawności</h3>

          <p>
            dzięki kalendarzowi zawodów <b>zaplanujesz wszystkie wydarzenia sportowe na cały rok</b>{' '}
          </p>

          <p className="cards__panel-padding">
            nie zapomnisz o żadnym wyjeździe i w porę przygotujesz się do zawodów
          </p>
          <p className="cards__panel-padding">
            testy sprawności z opisem każdej próby sprawnościowej i wzorcowe wyniki
          </p>
          <ul>
            <li>Test Denisiuka</li>
            <li>Międzynarodowy Test Sprawności Fizycznej</li>
            <li>Test Chromińskiego</li>
            <p className="cards__panel-border">
              <span>NOWOŚĆ!</span>
            </p>
            <li>
              <b>Test FMS - zwizualizowany praktycznymi zdjęciami</b>
            </li>
          </ul>
        </div>
        <div className="cards__panel-image">
          <SliderThree />
        </div>
      </div>
    </section>
  );
};
export default SectionCards;
