import React from 'react';
import '../../../assets/style/Promo.scss';
import imageLeft from '../../../assets/images/promo-left.png';
import iconOne from '../../../assets/images/promo-icon-one.png';
import iconTwo from '../../../assets/images/promo-icon-two.png';
import iconThree from '../../../assets/images/promo-icon-three.png';
import iconFour from '../../../assets/images/promo-icon-four.png';

const SectionPromo = () => {
  return (
    <section id="start" className="promo">
      <div className="container">
        <div data-aos="fade-right" className="promo__left">
          <img src={imageLeft} alt="" />
          <div data-aos="fade-right" data-aos-delay="300" className="promo__left-price">
            <h3>CENA</h3>
            <h4>na początek wakacji:</h4>
            <p>
              <span className="price-color">
                <b>
                  69<small>zł </small>
                </b>
                <span>
                  79<small>zł</small>
                </span>
              </span>
            </p>
          </div>
        </div>
        <div data-aos="fade-left" className="promo__right">
          <div className="promo__right-title">
            <h3>Utrzymujemy</h3>
            <h2>NAJWYŻSZE STANDARDY</h2>
          </div>
          <div className="promo__right-list">
            <div className="list-element">
              <div className="element-icon">
                <img src={iconOne} alt="" />
              </div>
              <div className="element-text">
                <h3>NIEZNISZCZALNY</h3>
                <p>
                  <b>twarda,solidna,oprawa</b> - elegancka i odporna na wilgoć
                </p>
              </div>
            </div>
            <div className="list-element">
              <div className="element-icon">
                <img src={iconTwo} alt="" />
              </div>
              <div className="element-text">
                <h3>POSŁUSZNY</h3>
                <p>
                  <b>grzbiet na spirali</b> - nie sprężynuje, nie zamyka stron
                </p>
              </div>
            </div>
            <div className="list-element">
              <div className="element-icon">
                <img src={iconThree} alt="" />
              </div>
              <div className="element-text">
                <h3>KOMPLETNY</h3>
                <p>
                  <b>8 klas</b> - i wszystko w jednym miejscu
                </p>
              </div>
            </div>
            <div className="list-element">
              <div className="element-icon">
                <img src={iconFour} alt="" />
              </div>
              <div className="element-text">
                <h3>PAKOWNY</h3>
                <p>
                  <b>trwała kieszeń na zwolnienia lekarskie i inne dokumenty</b>
                </p>
              </div>
            </div>
          </div>
          <div className="promo__right-text">
            <h1>
              <b>OBECNY</b> NA LEKCJACH WF
            </h1>
          </div>
        </div>
        <div id="about" className="promo__next">
          <div>
            <h1>NIEZBĘDNIK NAUCZYCIELA WF</h1>
            <p>podzielony został na 3 działy</p>
          </div>
        </div>
      </div>
    </section>
  );
};
export default SectionPromo;
