import React from 'react';
import '../../../assets/style/Online.scss';
import imageOne from '../../../assets/images/online-one.jpg';
import imageTwo from '../../../assets/images/online-two.jpg';
import imageThree from '../../../assets/images/online-three.jpg';

const SectionFMS = () => {
  return (
    <section className="online">
      <div className="container">
        <div className="online__one">
          <div className="online__one-title">
            <h2>JAK</h2>
            <p>
              korzystać <br /> z zasobów online
            </p>
            <h2>?</h2>
          </div>
          <div className="online__one-image">
            <img src={imageOne} alt="" />
          </div>
          <div className="online__one-text">
            <p>
              Jeśli posiadasz <b>Niezbędnik Nauczyciela WF 2020/21</b> możesz <b>BEZPŁATNIE</b>{' '}
              utworzyć konto w serwisie internetowym.
            </p>
          </div>
        </div>
        <div className="online__two">
          <div className="online__two-text">
            <p>
              <b>Kody QR</b> zawarte w niezbędniku przekierują Cię bezpośrednio do{' '}
              <b>filmów, scenariuszy lub testu.</b>
            </p>
          </div>
          <div className="online__two-image">
            <img src={imageTwo} alt="" />
          </div>
        </div>
        <div className="online__three">
          <div className="online__three-image">
            <img src={imageThree} alt="" />
          </div>
          <div className="online__three-text">
            <p>
              Możesz także korzystać ze skróconych linków lub przeglądać zawarość serwisu
              samodzielnie
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};
export default SectionFMS;
