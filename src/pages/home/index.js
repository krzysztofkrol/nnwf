import React from 'react';
import ScrollUpButton from 'react-scroll-up-button';
import Navigation from '../../layouts/Navigation';
import Header from '../../layouts/Header';
import Footer from '../../layouts/Footer';
import SectionAbout from './parts/SectionAbout';
import SectionMaterials from './parts/SectionMaterials';
import SectionFMS from './parts/SectionFMS';
import SectionOnline from './parts/SectionOnline';
import SectionPromo from './parts/SectionPromo';
import SectionCards from './parts/SectionCards';
import SectionBuy from './parts/SectionBuy';
import GoUp from '../../components/GoUp';

function Home() {
  return (
    <>
      <Navigation />
      <Header />
      <SectionPromo />
      <SectionCards />
      <SectionAbout />
      <SectionMaterials />
      <SectionFMS />
      <SectionOnline />
      <SectionBuy />
      <Footer />
      <ScrollUpButton
        ContainerClassName="AnyClassForContainer"
        TransitionClassName="AnyClassForTransition"
        EasingType="linear"
        ShowAtPosition={200}
      >
        <GoUp />
      </ScrollUpButton>
    </>
  );
}

export default Home;
