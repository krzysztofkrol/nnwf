import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import cardTwoImageOne from '../assets/images/cards/2-1.jpg';
import cardTwoImageTwo from '../assets/images/cards/2-2.jpg';
import cardTwoImageThree from '../assets/images/cards/2-3.jpg';

const SliderTwo = () => {
  const settings = {
    dots: true,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <Slider {...settings}>
      <div>
        <img src={cardTwoImageOne} alt="" />
      </div>
      <div>
        <img src={cardTwoImageTwo} alt="" />
      </div>
      <div>
        <img src={cardTwoImageThree} alt="" />
      </div>
    </Slider>
  );
};

export default SliderTwo;
