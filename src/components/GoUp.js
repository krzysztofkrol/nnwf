import React from 'react';

const GoUp = () => {
  return (
    <div className="go__up">
      <i className="fas fa-chevron-up" />
    </div>
  );
};
export default GoUp;
