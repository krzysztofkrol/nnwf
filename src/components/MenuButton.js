import React, { Component } from 'react';

// eslint-disable-next-line react/prefer-stateless-function
class MenuButton extends Component {
  render() {
    return (
      // eslint-disable-next-line react/button-has-type

      // eslint-disable-next-line react/button-has-type
      <button
        // eslint-disable-next-line react/prop-types
        className={this.props.className}
        onClick={this.props.toggleClassName}
      >
        <em className="hamburger"> </em>
      </button>
    );
  }
}
export default MenuButton;
