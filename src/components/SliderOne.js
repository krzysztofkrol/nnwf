import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import cardOneImageOne from '../assets/images/cards/1-1.jpg';
import cardOneImageTwo from '../assets/images/cards/1-2.jpg';
import cardOneImageThree from '../assets/images/cards/1-3.jpg';

const SliderOne = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <Slider {...settings}>
      <div>
        <img src={cardOneImageOne} alt="" />
      </div>
      <div>
        <img src={cardOneImageTwo} alt="" />
      </div>
      <div>
        <img src={cardOneImageThree} alt="" />
      </div>
    </Slider>
  );
};

export default SliderOne;
