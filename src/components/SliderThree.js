import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import cardThreeImageOne from '../assets/images/cards/3-1.jpg';
import cardThreeImageTwo from '../assets/images/cards/3-2.jpg';
import cardThreeImageThree from '../assets/images/cards/3-3.jpg';

const SliderThree = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    dalay: 5,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <Slider {...settings}>
      <div>
        <img src={cardThreeImageOne} alt="" />
      </div>
      <div>
        <img src={cardThreeImageTwo} alt="" />
      </div>
      <div>
        <img src={cardThreeImageThree} alt="" />
      </div>
    </Slider>
  );
};

export default SliderThree;
