import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import axios from 'axios';
import { getSamoobrona } from '../data/actions/samoobrona.action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
class SamoobronaMenu extends Component {
  async componentDidMount() {
    const { getSamoobrona } = this.props;

    getSamoobrona();
  }
  render() {
    const { samoobrona } = this.props;
    return (
      <div className="video__menu">
        <div className="video__menu-head">
          <h2>Samoobrona</h2> <button>–</button>
        </div>
        <div className="video__menu-list">
          {samoobrona.samoobrona.map(({ id, slug, title }) => (
            <Link key={id} to={`/online/video/pilates/${id}/${slug}`}>
              {title}
            </Link>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { samoobrona } = state;

  return { samoobrona };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getSamoobrona: getSamoobrona,
    },
    dispatch,
  );
export default connect(mapStateToProps, mapDispatchToProps)(SamoobronaMenu);
