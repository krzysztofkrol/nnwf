import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import axios from 'axios';
import { getPilates } from '../data/actions/pilates.action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
class VideoMenu extends Component {
  async componentDidMount() {
    const { getPilates } = this.props;
    getPilates();
  }
  render() {
    const { pilates } = this.props;

    return (
      <div className="video__menu">
        <div className="video__menu-head">
          <h2>Pilates</h2> <button>–</button>
        </div>
        <div className="video__menu-list">
          {pilates.pilates.map(({ id, slug, title }) => (
            <Link key={id} to={`/online/video/pilates/${id}/${slug}`}>
              {title}
            </Link>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { pilates } = state;

  return { pilates };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getPilates: getPilates,
    },
    dispatch,
  );
export default connect(mapStateToProps, mapDispatchToProps)(VideoMenu);
