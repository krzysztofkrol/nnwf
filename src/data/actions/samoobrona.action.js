import {
  GET_SAMOOBRONA_REQUEST,
  GET_SAMOOBRONA_SUCCESS,
  GET_SAMOOBRONA_FAILURE,
} from '../constans';

export const getSamoobrona = () => async (dispatch) => {
  dispatch({
    type: GET_SAMOOBRONA_REQUEST,
  });
  try {
    const response = await fetch(`http://localhost:3000/videos?category=Samoobrona`);
    const data = await response.json();

    dispatch({
      type: GET_SAMOOBRONA_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: GET_SAMOOBRONA_FAILURE,
    });
  }
};
