import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from '../constans';
// import { alertActions } from './';
import history from '../../helpers/history';
import axios from 'axios';

export const login = (email, password) => async (dispatch) => {
  dispatch({ type: LOGIN_REQUEST });
  return fetch('http://localhost:3000/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({ email, password }),
  })
    .then((resp) => resp.json())
    .then((data) => {
      console.log(data);
      // login successful if there's a jwt token in the response
      if (data.message) {
        console.log(data.message);
        dispatch({ type: LOGIN_FAILURE });

        // Here you should have logic to handle invalid login credentials.
        // This assumes your Rails API will return a JSON object with a key of
        // 'message' if there is an error
      } else {
        console.log(data.accessToken);
        localStorage.setItem('token', data.accessToken);

        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            data,
          },
        });
        history.push('/online');
      }
    });
};
