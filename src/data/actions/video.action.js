import { GET_VIDEO_REQUEST, GET_VIDEO_SUCCESS, GET_VIDEO_FAILURE } from '../constans';

export const getVideo = (id) => async (dispatch) => {
  dispatch({
    type: GET_VIDEO_REQUEST,
  });
  try {
    const response = await fetch(`http://localhost:3000/videos/${id}`);

    const data = await response.json();

    dispatch({
      type: GET_VIDEO_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: GET_VIDEO_FAILURE,
    });
  }
};
