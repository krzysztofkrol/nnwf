import { GET_PILATES_REQUEST, GET_PILATES_SUCCESS, GET_PILATES_FAILURE } from '../constans';

export const getPilates = () => async (dispatch) => {
  dispatch({
    type: GET_PILATES_REQUEST,
  });
  try {
    const response = await fetch(`http://localhost:3000/videos?category=Pilates`);
    const data = await response.json();
    dispatch({
      type: GET_PILATES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: GET_PILATES_FAILURE,
    });
  }
};
