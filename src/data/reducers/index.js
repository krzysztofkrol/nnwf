import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import pilates from './pilates.reducer';
import samoobrona from './samoobrona.reducer';
import video from './video.reducer';

const rootReducer = combineReducers({
  authentication,
  pilates,
  samoobrona,
  video,
});

export default rootReducer;
