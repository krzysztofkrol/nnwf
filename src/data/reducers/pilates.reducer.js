import { GET_PILATES_REQUEST, GET_PILATES_SUCCESS } from '../constans';

const initialState = {
  pilates: [],
};

function pilates(state = initialState, action) {
  switch (action.type) {
    case GET_PILATES_REQUEST:
      return {
        ...state,
      };
    case GET_PILATES_SUCCESS:
      return {
        ...state,
        pilates: [...action.payload],
      };

    default:
      return state;
  }
}

export default pilates;
