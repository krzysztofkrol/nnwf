import { GET_SAMOOBRONA_REQUEST, GET_SAMOOBRONA_SUCCESS } from '../constans';

const initialState = {
  samoobrona: [],
};

function samoobrona(state = initialState, action) {
  switch (action.type) {
    case GET_SAMOOBRONA_REQUEST:
      return {
        ...state,
      };
    case GET_SAMOOBRONA_SUCCESS:
      return {
        ...state,
        samoobrona: [...action.payload],
      };

    default:
      return state;
  }
}

export default samoobrona;
