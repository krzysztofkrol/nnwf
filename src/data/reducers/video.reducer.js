import { GET_VIDEO_REQUEST, GET_VIDEO_SUCCESS } from '../constans';

const initialState = {
  video: [],
};

function video(state = initialState, action) {
  switch (action.type) {
    case GET_VIDEO_REQUEST:
      return {
        ...state,
      };
    case GET_VIDEO_SUCCESS:
      return {
        ...state,
        video: action.payload,
      };

    default:
      return state;
  }
}

export default video;
